// Select our canvas and set context to 2d
// Context is the actual place we draw shapes on
var canvas = document.querySelector('canvas');
var c = canvas.getContext('2d');

// canvas size same as window size
var width = canvas.width = window.innerWidth;
var height = canvas.height = window.innerHeight;

// Mouse object
// while no values given, its undefined
var mouse = {
    x: undefined,
    y: undefined
}

// Max radius of the circle
var maxRadius = 100;
var mouseRadius = 50;

// Color array
var colorArray = [
  '#ffaa22',
  '#99ffaa',
  '#00ff00',
  '#4411aa',
  '#ff1100',
  '#F0FE7B',
  '#08CDE4',
  '#76234E'
];

// Circle object constructor
function Circle(x, y, dx, dy, radius) {
  this.x = x;
  this.y = y;
  this.dx = dx;
  this.dy = dy;
  this.radius = radius;
  this.minRadius = radius;
  this.color = colorArray[Math.floor(Math.random() * colorArray.length)];

  // Draw circle
  this.draw = function() {
    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
    c.fillStyle = this.color;
    c.fill();
  }

  // Update circle movement
  this.update = function() {
    if(this.x + this.radius > width 
      || this.x - this.radius < 0) {
      this.dx = -this.dx;
    }

    if(this.y + this.radius > height 
      || this.y - this.radius < 0) {
      this.dy = -this.dy;
    }

    this.x += this.dx;
    this.y += this.dy;

    // interactivity
    if(mouse.x - this.x < mouseRadius && mouse.x - this.x > -mouseRadius
      && mouse.y - this.y < mouseRadius && mouse.y - this.y > -mouseRadius) {
      if(this.radius < maxRadius) {
        this.radius += 3;
      }
      this.x += this.dx;
      this.y += this.dy;
    } else if(this.radius > this.minRadius) {
      this.radius -= 1;
    }


    this.draw();
  }

}

window.addEventListener('mousemove', function(e) {
  mouse.x = e.clientX;
  mouse.y = e.clientY;
});

window.addEventListener('resize', function() {
  width = canvas.width = window.innerWidth;
  height = canvas.height = window.innerHeight;

  init();
});

var circleArray = [];

function init() {

  circleArray = [];

  for(var i = 0; i < 1000; i++) {
    var radius = Math.random() * 15 + 1;

    var x = Math.random() * (width - radius * 2) + radius;
    var y = Math.random() * (width - radius * 2) + radius;

    var dx= (Math.random() - 0.5);
    var dy= (Math.random() - 0.5);

    circleArray.push(new Circle(x, y, dx, dy, radius));
  }

  animate();
}

function animate() {
  requestAnimationFrame(animate);

  c.clearRect(0, 0, width, height);

  for(var i = 0; i < circleArray.length; i++) {
    circleArray[i].update();
  }
}

init();
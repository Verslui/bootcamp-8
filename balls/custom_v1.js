var canvas = document.querySelector('canvas');
var c = canvas.getContext('2d');

var width = canvas.width = window.innerWidth;
var height = canvas.height = window.innerHeight;

function random(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + 1;
}

function Ball(x, y, velX, velY, color, size) {
	this.x = x;
	this.y = y;
	this.velX = velX;
	this.velY = velY;
	this.color = color;
	this.size = size;
}

Ball.prototype.draw = function() {
	c.beginPath();
	c.fillStyle = this.color;
	c.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
	c.fill();
}

Ball.prototype.update = function() {
	if((this.x + this.size) >= width) {
		this.velX = -this.velX;
	}

	if((this.x + this.size) <= 0) {
		this.velX = -this.velX;
	}

	if((this.y + this.size) >= height) {
		this.velY = -this.velY;
	}

	if((this.y + this.size) <= 0) {
		this.velY = -this.velY;
	}

	this.x += this.velX;
	this.y += this.velY;
}

// Ball.prototype.draw = function(e) {
// 	c.clearRect(0, 0, width, height);
// 	c.beginPath();
// 	c.fillStyle = this.color;
// 	c.arc(e.x, e.y, this.size, 0, 2 * Math.PI);
// 	c.fill();
// }

// window.addEventListener('mousemove', function(e) {
// 	ball.draw(e);
// });

var balls = [];

var ballsCount = random(500, 1500)
console.log(ballsCount);
for (var i = 0; i < ballsCount ; i++) {
		var ball = new Ball(
			random(0, width),
			random(0, height),
			random(-3, 3),
			random(-10, 10),
			'rgb(' + random(0, 255) + ', ' + random(0, 255) + ', ' + random(0, 255) + ')',
			random(10, 30)
		);

		balls.push(ball);
}

function init() {

	for(var i = 0; i < balls.length; i++) {
		balls[i].draw();
		balls[i].update();
	}

	requestAnimationFrame(init);
}

init();
